import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from "@react-navigation/drawer";
import {MaintabScreen} from "./screens/MainTabScreen";
import {DrawerContent} from "./screens/DrawerContent";
import {RootStackScreen} from "./screens/RootStackScreen";

const Drawer = createDrawerNavigator();

export default function App() {
    return (
        <NavigationContainer>
            {/*<Drawer.Navigator drawerContent={props => <DrawerContent {...props}/>}>*/}
            {/*    <Drawer.Screen name={"HomeDrawer"} component={MaintabScreen}/>*/}
            {/*    /!*<Drawer.Screen name={"Details"} component={DetailsStackScreen}/>*!/*/}
            {/*</Drawer.Navigator>*/}
            <RootStackScreen/>
        </NavigationContainer>
    );
}
